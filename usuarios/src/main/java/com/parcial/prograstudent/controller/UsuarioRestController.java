package com.parcial.prograstudent.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.parcial.prograstudent.Entity.Usuario;
import com.parcial.prograstudent.service.UsuarioService;
//Indiciamos que es un controlador rest
@RestController
@RequestMapping("/parcial") //esta sera la raiz de la url, es decir http://127.0.0.1:8080/api/

public class UsuarioRestController {
	
	//Inyectamos el servicio para poder hacer uso de el
	@Autowired
	private UsuarioService UsuarioService;

	/*Este método se hará cuando por una petición GET (como indica la anotación) se llame a la url 
	http://127.0.0.1:8080/api/users*/
	@GetMapping("/usuarios")
	public List<Usuario> findAll(){
		//retornará todos los usuarios
		return UsuarioService.findAll();
	}
	
	@GetMapping("/usuarios/{userId}")
	public Usuario getUser(@PathVariable int userId) {
	Usuario user = UsuarioService.findById(userId);
	if (user == null) {
	throw new ResponseStatusException(
	HttpStatus.NOT_FOUND, "No Existe el usuario");
	}
	return user;
	}
	///agregar usuarios
	@PostMapping("/usuarios")
	public Usuario addUser(@RequestBody Usuario user) {
	user.setId(0);
	UsuarioService.save(user);
	return user;
	}
	
	@PutMapping("/usuarios/{userId}")
	public Usuario updateUser(@RequestBody Usuario user, @PathVariable int userId) {
	user.setId(userId);
	UsuarioService.save(user);
	return user;
	}
	
	@DeleteMapping("/usuarios/{userId}")
	  void deleteUser(@PathVariable int userId) {
	    UsuarioService.deleteById(userId);
	  }
	
	
	
}