package com.parcial.prograstudent.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.parcial.prograstudent.Entity.Usuario;
import com.parcial.prograstudent.dao.UsuarioDao;

 

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioDao UsuarioDAO;
	
	@Override
	public List<Usuario> findAll() {
		List<Usuario> listUsers= UsuarioDAO.findAll();
		return listUsers;
	}

	@Override
	public Usuario findById(int id) {
		Usuario user = UsuarioDAO.findById(id);
		return user;
	}

	@Override
	public void save(Usuario user) {
		UsuarioDAO.save(user);

	}

	@Override
	public void deleteById(int id) {
		UsuarioDAO.delete(id);
	}

}