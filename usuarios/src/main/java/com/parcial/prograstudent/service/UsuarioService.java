package  com.parcial.prograstudent.service;

import java.util.List;

import com.parcial.prograstudent.Entity.Usuario;

public interface UsuarioService {
	
	public List<Usuario> findAll();
	
	public Usuario findById(int id);
	
	public void save (Usuario user);
	
	public void deleteById (int id);

}
