package com.parcial.prograstudent.dao;
import java.util.List;


import com.parcial.prograstudent.Entity.Usuario;

public interface UsuarioDao {
	public List<Usuario> findAll();
	
	public Usuario findById(int id);
	
	public void save (Usuario user);
	
	public void delete (int id);
}
