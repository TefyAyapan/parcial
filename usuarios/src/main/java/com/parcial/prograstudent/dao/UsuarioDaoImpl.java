package com.parcial.prograstudent.dao;
import org.springframework.transaction.annotation.Transactional;

import com.parcial.prograstudent.Entity.Usuario;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
@Transactional
@Repository
public class UsuarioDaoImpl implements UsuarioDao {
	
	
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Usuario> findAll() {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Usuario> selectQuery = currentSession.createQuery("from usuarios", Usuario.class);
		List<Usuario> usuarios = selectQuery.getResultList();
		return usuarios;
	}

	@Override
	public Usuario findById(int id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Usuario user = currentSession.get(Usuario.class, id);
		return user;
	}

	@Override
	public void save(Usuario user) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.saveOrUpdate(user);
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Usuario> deleteQuery = currentSession.createQuery("delete from usuarios where id=:idUser");
		deleteQuery.setParameter("idUser", id);
		deleteQuery.executeUpdate();

	}


}
